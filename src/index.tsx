import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

const renderReactDom = () => {
  ReactDOM.render(<App />, document.getElementById("root"));
};

if ("cordova" in window) {
  document.addEventListener("deviceready", renderReactDom, false);
} else {
  renderReactDom();
}

reportWebVitals();
